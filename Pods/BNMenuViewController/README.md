# BNMenuViewController

[![Version](https://img.shields.io/cocoapods/v/BNMenuViewController.svg?style=flat)](https://cocoapods.org/pods/BNMenuViewController)
[![License](https://img.shields.io/cocoapods/l/BNMenuViewController.svg?style=flat)](https://cocoapods.org/pods/BNMenuViewController)
[![Platform](https://img.shields.io/cocoapods/p/BNMenuViewController.svg?style=flat)](https://cocoapods.org/pods/BNMenuViewController)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BNMenuViewController is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'BNMenuViewController'
```

## Author

Bijesh Nair, bijesh4@gmail.com

## License

BNMenuViewController is available under the MIT license. See the LICENSE file for more info.
