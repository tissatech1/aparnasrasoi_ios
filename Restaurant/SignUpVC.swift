//
//  SignUpVC.swift
//  Restaurant
//
//  Created by TISSA Technology on 10/31/20.
//

import UIKit
import Alamofire

class SignUpVC: UIViewController {

    @IBOutlet var salutTxt: UITextField!
    @IBOutlet var userNameTxt: UITextField!
    @IBOutlet var firstNameTxt: UITextField!
    @IBOutlet var lastNameTxt: UITextField!
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet var codeTxt: UITextField!
    @IBOutlet var mobileNumTxt: UITextField!
    @IBOutlet var passwordTxt: UITextField!
    @IBOutlet var confpasswordTxt: UITextField!
    @IBOutlet var blurView: UIView!

    
    
    var regid = Int()
    var usernameget = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: true)
        blurView.isHidden = true

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.view.alpha = 0.7
        UIView.animate(withDuration: 1.5, animations: {
                self.view.alpha = 1.0
            })
    }
    

    @IBAction func loginBtnClicked(_ sender: UIButton) {
        

        self.navigationController?.popViewController(animated: false)

        
    }
    
    @IBAction func SubmitBtnClicked(_ sender: UIButton) {
    
        if salutTxt.text == "" ||  salutTxt.text == nil {
            
            showSimpleAlert(messagess: "Enter salutation")
            
        }else if userNameTxt.text == "" ||  userNameTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter user name")

        }else if firstNameTxt.text == "" ||  firstNameTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter first name")

        }else if lastNameTxt.text == "" ||  lastNameTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter last name")

        }else if emailTxt.text == "" ||  emailTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter email")

        }else if mobileNumTxt.text == "" ||  mobileNumTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter mobile number")

        }else if passwordTxt.text == "" ||  passwordTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter password")

        }else if passwordTxt.text!.count < 7 {
            
            showSimpleAlert(messagess: "Password must be greater than or equal to characters")

        }else if confpasswordTxt.text == "" || confpasswordTxt.text == nil{
            
            showSimpleAlert(messagess: "Enter confirm password")
        }else if passwordTxt.text! != confpasswordTxt.text!{
            
            showSimpleAlert(messagess: "Password and confirm password does not match")
        }else if emailTxt.text?.isValidEmail == false{
            
            showSimpleAlert(messagess: "Enter valid email")
        }else{
        
        
       ERProgressHud.sharedInstance.show(withTitle: "Loading...")
       adminlogincheck()
            
        }
        
    
    }

    //MARK: Admin Login
    
    func adminlogincheck(){

        let urlString = GlobalObjects.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalObjects.adminusername, "password":GlobalObjects.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            
                            self.signupApi()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    
    func signupApi()  {
        
        let defaults = UserDefaults.standard
        
        let admintoken = defaults.object(forKey: "adminToken")as? String
      
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalObjects.DevlopmentApi + "user/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .post, parameters: ["username":userNameTxt.text!, "email":emailTxt.text!,"first_name":firstNameTxt.text!,"last_name":lastNameTxt.text!,"password":passwordTxt.text!,"verified":"N","restaurant_id":GlobalObjects.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    print(dict)
                                    
                                    regid = dict["id"]as! Int
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                                    createcustomerApi()
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        
                                        self.showSimpleAlert(messagess: "User exist in system with same email or username")
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }         
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    
    
    func createcustomerApi()  {
        
        
        let now = Date()

            let formatter = DateFormatter()

            formatter.timeZone = TimeZone.current

            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS" ///2020-10-19 12:05:10.625

            let dateString = formatter.string(from: now)
        
        print(dateString)
        
        let phnumber = codeTxt.text! + mobileNumTxt.text!
        
        
        let defaults = UserDefaults.standard
        
        let admintoken = defaults.object(forKey: "adminToken")as? String
      let customeridStr = String(regid)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalObjects.DevlopmentApi + "customer/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "action": "customer"
            ]

        AF.request(urlString, method: .post, parameters: ["customer_id":regid, "last_access":dateString,"extra":"extra","salutation":salutTxt.text!,"phone_number":phnumber,"first_name":firstNameTxt.text!,"last_name":lastNameTxt.text!,"email":emailTxt.text!],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    print(dict)
                                    
                                    let alert = UIAlertController(title:"Registration has been done successfully" , message: "We sent you an email for account verification",         preferredStyle: UIAlertController.Style.alert)

                                  
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    
                                        ERProgressHud.sharedInstance.hide()
                                                                   

            self.navigationController?.popViewController(animated: false)

                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor(rgb: 0xFC4355)
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        
                                        self.showSimpleAlert(messagess: "User exist in system with email or username")
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                              
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFC4355)
    }
    
    
    @IBAction func salutBtnClicked(_ sender: UIButton) {
        
        blurView.isHidden = false

        
    }
    
    @IBAction func mrBtnClicked(_ sender: UIButton) {
        blurView.isHidden = true
        salutTxt.text = "Mr."
    }
    
    @IBAction func mrsBtnClicked(_ sender: UIButton) {
        blurView.isHidden = true
        salutTxt.text = "Mrs."
    }
    
    @IBAction func missBtnClicked(_ sender: UIButton) {
        blurView.isHidden = true
        salutTxt.text = "Miss."
       
    }
    
    @IBAction func msBtnclicked(_ sender: UIButton) {
        blurView.isHidden = true
        salutTxt.text = "Ms."
       
    }
    
    
    
    
}
extension String {
    var isValidEmail: Bool {
        NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: self)
    }
}
