//
//  PaymentPage.swift
//  Restaurant
//
//  Created by TISSA Technology on 11/24/20.
//

import UIKit
import Alamofire

class PaymentPage: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource,UIPopoverPresentationControllerDelegate,UITextFieldDelegate,UITextViewDelegate {

    @IBOutlet weak var subtotalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var shippingfeeLbl: UILabel!
    @IBOutlet weak var servicefeelbl: UILabel!
    @IBOutlet weak var tipLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var iBtn: UIButton!
    @IBOutlet weak var paymentNextBtn: UIButton!
    @IBOutlet weak var changeBtn: UIButton!
    @IBOutlet weak var textbaseView: UIView!
    @IBOutlet weak var textbaseViewTf: UITextView!
    @IBOutlet weak var changeviewouter: UIView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var changeViewBase: UIView!
    @IBOutlet weak var title1Lbl: UILabel!
    @IBOutlet weak var title2Lbl: UILabel!
    @IBOutlet weak var per5Lbl: UILabel!
    @IBOutlet weak var per10Lbl: UILabel!
    @IBOutlet weak var rup20Lbl: UILabel!
    @IBOutlet weak var rup50Lbl: UILabel!
    @IBOutlet weak var otherLbl: UILabel!
    @IBOutlet weak var otherTfouterView: UIView!
    @IBOutlet weak var otherTf: UITextField!
    @IBOutlet weak var redio5perBtn: UIButton!
    @IBOutlet weak var redio10perBtn: UIButton!
    @IBOutlet weak var redio20RupeeBtn: UIButton!
    @IBOutlet weak var redio50RupeeBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    @IBOutlet weak var changeSubmitBtn: UIButton!
    @IBOutlet weak var closeBTn: UIButton!
    @IBOutlet weak var infoOuterView: UIView!
    @IBOutlet weak var cardholderView: UIView!
    @IBOutlet weak var cardnumberView: UIView!
    @IBOutlet weak var securitycodeView: UIView!
    @IBOutlet weak var monthView: UIView!
    @IBOutlet weak var yearView: UIView!
    @IBOutlet weak var monthBtn: UIButton!
    @IBOutlet weak var yearBtn: UIButton!
    @IBOutlet weak var cardholdernameTF: UITextField!
    @IBOutlet weak var creditcardnumberTF: UITextField!
    @IBOutlet weak var securityTF: UITextField!
    @IBOutlet weak var datepickerouterView: UIView!
    @IBOutlet weak var piockergreenview: UIView!
    @IBOutlet weak var greenselectrLbl: UILabel!
    @IBOutlet weak var pickerinnerView: UIView!
    @IBOutlet weak var pickerDateView: UIPickerView!
    @IBOutlet weak var yearpicker: UIPickerView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var pickercancelBtn: UIButton!
    @IBOutlet weak var pickerOkBtn: UIButton!
   
    var firsttimefeeData = NSArray()
    var getcartarray = NSArray()
    var parametername = String()
    var tipvalue = String()
    var otherclick = String()
    var redioclicked = String()
    
    var discoutStr = String()
    var servicefeeStr = String()
    var shippoingfeeStr = String()
    var subtotalStr = String()
    var taxStr = String()
    var totalStr = String()
    var tipStr = String()
    
    var orderresultCurrency = String()
    var orderresultamount = String()
    var orderresultorderid = Int()
    var orderresultcustomerid = Int()
    var customerPhoneNumber = String()
    var customerEmailId = String()
    var stringoOfCArdNO = String()
    var stringOfACardCVC = String()
    var stingOFCardMonth = String()
    var stringOfCardYear = String()
    var YearArr = NSArray()
    var whichbtn = String()
    var dates =  [Date]()
    var shippingmethodStr = String()
    var paymentmethodStr = String()
    
    var months: Array<String> = Calendar.current.monthSymbols
    var orderplaced = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        orderplaced = "no"
        
        stringOfCardYear = ""
        stringOfCardYear = ""
        
        monthBtn.layer.cornerRadius = 6
        monthBtn.layer.borderWidth = 1
        monthBtn.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
        yearBtn.layer.cornerRadius = 6
        yearBtn.layer.borderWidth = 1
        yearBtn.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
        infoOuterView.layer.cornerRadius = 6
        infoOuterView.layer.borderWidth = 1
        infoOuterView.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
        cardholderView.layer.cornerRadius = 6
        cardholderView.layer.borderWidth = 1
        cardholderView.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
        cardnumberView.layer.cornerRadius = 6
        cardnumberView.layer.borderWidth = 1
        cardnumberView.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
        securitycodeView.layer.cornerRadius = 6
        securitycodeView.layer.borderWidth = 1
        securitycodeView.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
        changeviewouter.layer.cornerRadius = 6
        changeviewouter.layer.borderWidth = 1
        changeviewouter.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
        textbaseView.layer.cornerRadius = 6
        textbaseView.layer.borderWidth = 1
        textbaseView.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
        paymentNextBtn.layer.cornerRadius = 6
        self.viewhide()
        parametername = "tip"
        tipvalue = "0"
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        fisrttimefeeapi()
        setupToolbar()
        getcustomer()
     
//        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
//        getcustomer()
        
//        let defaults = UserDefaults.standard
//
//        let cartarray:NSArray = defaults.object(forKey: "cartarray")as! NSArray
//
//        print("cart array - \(cartarray)")
//
//        var equipments = [[String:Any]]()
//
//        for i in 0 ..< cartarray.count  {
//
//            let firstobj:NSDictionary  = cartarray.object(at: i) as! NSDictionary
//
//            equipments.append([ "cart_item_id":firstobj["cart_item_id"]as! Int,
//                                "cart_id":firstobj["cart_id"]as! Int,
//                                "product_id":firstobj["product_id"]as! Int,
//                                "product_name":firstobj["product_name"]as! String,
//                                "product_url":firstobj["product_url"]as! String,
//                                "price":firstobj["price"]as! String ,
//                                "quantity":firstobj["quantity"]as! Int,
//                                "line_total":firstobj["line_total"]as! String,
//                                "canceled":false])
//
//            self.getcartarray = equipments as NSArray
    //    }
        
        
    //    print(" final cart array - \(getcartarray)")
        

        
        
//        let billinfo:NSDictionary = defaults.object(forKey: "billingaddressDICT")as! NSDictionary
//
//        print("billinfo dict - \(billinfo)")
        

        
        YearArr = ["2020","2021","2022","2023","2024","2025","2026","2027","2028","2029","2030","2031","2032","2033","2034","2035","2036","2037","2038","2039","2040","2041"]


        pickerDateView.delegate = self
        pickerDateView.dataSource = self
        yearpicker.delegate = self
        yearpicker.dataSource = self
        
        pickerhide()
        
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       
        
        if pickerView == yearpicker {
            return YearArr.count
        }
        
        if pickerView == pickerDateView {
            return months.count
        }
        

        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == yearpicker {
            return (YearArr[row] as! String)
        }
        
        if pickerView == pickerDateView {
            return months[row]
        }
   
        return ""

    }


    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerDateView{
        
        let monthStr = months[row]
        var monthnum = String()

        if monthStr == "January" {
            monthnum = "01"
            stingOFCardMonth = "01"
        }else if monthStr == "February" {
            monthnum = "02"
            stingOFCardMonth = "02"
        }else if monthStr == "March" {
            monthnum = "03"
            stingOFCardMonth = "03"
        }else if monthStr == "April" {
            monthnum = "04"
            stingOFCardMonth = "04"
        }else if monthStr == "May" {
            monthnum = "05"
            stingOFCardMonth = "05"
        }else if monthStr == "June" {
            monthnum = "06"
            stingOFCardMonth = "06"
        }else if monthStr == "July" {
            monthnum = "07"
            stingOFCardMonth = "07"
        }else if monthStr == "August" {
            monthnum = "08"
            stingOFCardMonth = "08"
        }else if monthStr == "September" {
            monthnum = "09"
            stingOFCardMonth = "09"
        }else if monthStr == "October" {
            monthnum = "10"
            stingOFCardMonth = "10"
        }else if monthStr == "November" {
            monthnum = "11"
            stingOFCardMonth = "11"
        }else if monthStr == "December" {
            monthnum = "12"
            stingOFCardMonth = "12"
        }


        monthBtn.setTitle(monthnum, for: UIControl.State.normal)

            
        }else{
            
            let yearSTr = YearArr[row]
            yearBtn.setTitle((yearSTr as! String), for: UIControl.State.normal)
            stringOfCardYear = yearSTr as! String
        }
        
        
       }
    
    @IBAction func paymentbackarrow(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func iBtnClicked(_ sender: Any) {
        
     
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "popoverVC") as? popoverVC
        vc!.preferredContentSize = CGSize(width: 350, height: 70)
        vc!.modalPresentationStyle = .popover

        if let pres = vc?.presentationController {
                    pres.delegate = self
                }
        self.present(vc!, animated: true)
        if let pop = vc?.popoverPresentationController {
//                    pop.sourceView = (sender as! UIView)
//                    pop.sourceRect = (sender as! UIView).bounds
                    
                    pop.sourceView = (sender as! UIView)
                    pop.sourceRect = (sender as! UIView).bounds
                    
                }
        
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
    return .none
    }

    //UIPopoverPresentationControllerDelegate
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {

    }

    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
    return true
    }
  
    
    @IBAction func paymentNextBtnClicked(_ sender: Any) {
        
        if cardholdernameTF.text == "" || cardholdernameTF.text == nil {
            showSimpleAlert(messagess: "Enter card holder name")
        }else if creditcardnumberTF.text == "" || creditcardnumberTF.text == nil {
            showSimpleAlert(messagess: "Enter card number")
        }else if securityTF.text == "" || securityTF.text == nil {
            showSimpleAlert(messagess: "Enter security code")
        }else if stingOFCardMonth == "" || stingOFCardMonth.isEmpty {
            showSimpleAlert(messagess: "Enter month")
        }else if stringOfCardYear == "" || stringOfCardYear.isEmpty {
            showSimpleAlert(messagess: "Enter year")
        }else{
            
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
            
            if orderplaced == "yes" {
                
            paymentApi()
                
            }else{
                
          orderDetailApi()
                
            }
        }
        
    }
    

    func fisrttimefeeapi() {
        
        
         let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)

        let getshippingId = defaults.object(forKey: "clickedShippingMethodId")as! Int
        
        var subtotalStrfee = String()
        var notaxtotalstr = String()
     //  let carttotal = defaults.object(forKey: "totalcartPrice")as! String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let cartarray:NSArray = defaults.object(forKey: "cartarray")as! NSArray

        print("cart array - \(cartarray)")
        
       // var sumnotax = 0.00
        var sumsub = 0.00
           
        notaxtotalstr = "0.00"
        subtotalStrfee = "0.00"
        
        for i in 0 ..< cartarray.count  {

         //   let datagetobject:NSDictionary  = cartarray.object(at: i) as! NSDictionary
        
           
//            if (datagetobject["tax_exempt"] as! Bool == true) {
//
//                let countobj:NSDictionary  = cartarray.object(at: i) as! NSDictionary
//
//                let linetotal = countobj["line_total"] as! String
//
//
//                let qnt = Double(linetotal)
//
//
//                sumnotax = sumnotax + qnt!
//
//                notaxtotalstr = String(format: "%.2f", sumnotax)
//
//
//
//            }else{
//
               
                
//                let countobj:NSDictionary  = cartarray.object(at: i) as! NSDictionary
//
//                let linetotal = countobj["line_total"] as! String
//
//
//                let qnt = Double(linetotal)
//
//
//                sumsub = sumsub + qnt!
//
//
//                subtotalStrfee = String(format: "%.2f", sumsub)

          //  }
        
        
        }
        
        let passedcartprice = defaults.object(forKey: "totalcartPrice")as? String
        
        subtotalStrfee = passedcartprice!
    
        print("customerId - \(customerId)")
        print("getshippingId - \(getshippingId)")
        print("carttotal - \(subtotalStrfee)")
        
        print("no_tax_total - \(notaxtotalstr)")
        print("sub_total - \(subtotalStrfee)")
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "fee"
            ]

               let urlString = GlobalObjects.DevlopmentApi+"fee/"

        AF.request(urlString, method: .post, parameters: ["sub_total": subtotalStrfee, "no_tax_total": notaxtotalstr,"customer_id": customerId,parametername: tipvalue],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
               response in
                 switch response.result {
                               case .success:
                                print(response)

                                   if response.response?.statusCode == 200{
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    
                                    
                                     print("firstTimefee Result - \(dict)")
                                       
   
                                        self.discountLbl.text = "$\((dict["discount"] as! String))"
                                        discoutStr = (dict["discount"] as! String)
                                        
                                self.servicefeelbl.text = "$\((dict["service_fee"] as! String))"
                                        servicefeeStr = (dict["service_fee"] as! String)

                                        
                                self.shippingfeeLbl.text = "$\((dict["shipping_fee"] as! String))"
                                        shippoingfeeStr = (dict["shipping_fee"] as! String)


                            self.subtotalLbl.text = "$\((dict["sub_total"] as! String))"
                                        subtotalStr = (dict["sub_total"] as! String)

                                        
                            self.taxLbl.text = "$\((dict["tax"] as! String))"
                                        taxStr = (dict["tax"] as! String)

                                        
                            self.totalLbl.text = "$\((dict["total"] as! String))"
                                        totalStr = (dict["total"] as! String)

                                        
                        self.tipLbl.text = "$\((dict["tip"] as! String))"
                                        tipStr = (dict["tip"] as! String)
 
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    
                                   }else{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 401{
                                        
                                        ERProgressHud.sharedInstance.hide()
                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                
                                    
                                   }
                                   
                                   break
                               case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                               }
               }


            }
    
    
    
    
    
    func orderDetailApi() {


       let discountdata = discoutStr

        let servicefeedata = servicefeeStr

      //  let servicefeedata = "0.00"

        let shippingfeedata = shippoingfeeStr

        let subtotaldata = subtotalStr

        let taxdata = taxStr

        let totaldata =  totalStr

        let tipdata = tipStr
        
        var commentStr = String()
        
        if textbaseViewTf.text == nil || textbaseViewTf.text! == "" {
            commentStr = ""
        }else{
            
            commentStr = textbaseViewTf.text! as String
        }
        
       print("commentStr - \(commentStr)")

      
          let  cuurency = "USD"
        

       // let cuurency = globelObjectVC.countryfixglob
        print("get currency - \(cuurency)")
        var customrtid = String()

         let defaults = UserDefaults.standard

     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)

        customrtid = String(customerId)

        let getshippingId = defaults.object(forKey: "clickedShippingMethodId")as! Int
       let carttotal = defaults.object(forKey: "totalcartPrice")as! String

        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"


        print("customerId - \(customerId)")
        print("getshippingId - \(getshippingId)")
        print("carttotal - \(carttotal)")

        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)

            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "order-detail"
            ]

               let urlString = GlobalObjects.DevlopmentApi+"order-detail/"

        AF.request(urlString, method: .post, parameters: ["status": "active", "currency": cuurency,"subtotal": subtotaldata,"total": totaldata,"extra": commentStr, "customer": customrtid,"tip": tipdata,"service_fee": servicefeedata,"tax": taxdata,"discount": discountdata,"shipping_fee": shippingfeedata,"cart_id": cartidStr],encoding: JSONEncoding.default, headers: headers).responseJSON {
               response in
                 switch response.result {
                               case .success:
                                 //  print(response)

                                   if response.response?.statusCode == 200{

                                    self.orderplaced = "yes"
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary

                                     print("order detail Result - \(dict)")

                                    let defaults = UserDefaults.standard
                                    defaults.set(dict, forKey: "passOrderResult")

                                    self.orderresultCurrency = (dict["currency"] as! String)
                                    self.orderresultamount = (dict["total"] as! String)
                                    self.orderresultorderid = (dict["order_id"] as! Int)
                                    self.orderresultcustomerid = (dict["customer"] as! Int)


                                   
                                   // self.show()
                                    self.paymentApi()
                                    
                                   
                                    

                                   }else{

                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 401{

                                        ERProgressHud.sharedInstance.hide()
                                        self.SessionAlert()

                                    }else if response.response?.statusCode == 500{

                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary

                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }



                                   }

                                   break
                               case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                               }
               }


            }

    
    
    func getcustomer()  {
        
        let defaults = UserDefaults.standard
        
        let savedUserData = defaults.object(forKey: "custToken")as? String
        
        let customerid = defaults.integer(forKey: "custId")
        let custidStr = String(customerid)
        
        let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
            
        let urlString = GlobalObjects.DevlopmentApi+"customer/?customer_id="+custidStr+""
        print("Url cust avl - \(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": token
            ]

             AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                
                                print(response)

                                if response.response?.statusCode == 200{
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                                //   print(dict)
                                    
                                    let status = dict.value(forKey: "results")as! NSArray
                                                 //  print(status)

                                        print("customer detail - \(status)")

                                    let firstobj:NSDictionary  = status.object(at: 0) as! NSDictionary
                                    
                                    self.customerPhoneNumber = firstobj["phone_number"] as! String
                                    
                                    print(self.customerPhoneNumber)
                                    
                                    let customerinfo:NSDictionary = firstobj.value(forKey: "customer")as! NSDictionary
                                    
                                    
                                    
                                    self.customerEmailId = customerinfo["email"] as! String
                                    
                                //    self.customerEmailId = ""
                                    
                                    print(self.customerEmailId)
                                    
                      }else{
                                    
                                //    self.dissmiss()
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        
                                    }else if response.response?.statusCode == 401{
                                        ERProgressHud.sharedInstance.hide()

                                    self.SessionAlert()
                                        
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                            
                                print(response)
                                }
                                
                                break
                            case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    
    func paymentApi()  {
        
        
        let defaults = UserDefaults.standard
       
    //   let admintoken = defaults.object(forKey: "adminToken")as? String
       let admintoken = defaults.object(forKey: "custToken")as? String
       let customerId = defaults.integer(forKey: "custId")
        let  customeridStr = String(customerId)

        
       let getshippingId = defaults.object(forKey: "clickedShippingMethodId")as! Int
        let shippingmethodName = defaults.object(forKey: "clickedShippingMethod")as! String
        
        print(shippingmethodName)
        let shippingmethodID = defaults.integer(forKey: "clickedShippingMethodId")
        
       let storepassid = (defaults.object(forKey: "clickedStoreId")as? String)!
        
        print(storepassid)
       
      
      
       let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

      
        
        var metadataDict = [String : Any]()
        
        metadataDict = ["order_id":orderresultorderid, "restaurant_id":storepassid, "customer_id":orderresultcustomerid, "shippingmethod_id":shippingmethodID,"phone":customerPhoneNumber]
        
        print(metadataDict)
        
        
        
        var cardDataDict = [String : Any]()
        
        cardDataDict = ["number":creditcardnumberTF.text!, "exp_month":stingOFCardMonth,"exp_year":stringOfCardYear, "cvc":securityTF.text!]
        
        print(cardDataDict)
        
        
        let billinfo:NSDictionary = defaults.object(forKey: "billingaddressDICT")as! NSDictionary

        print("billinfo dict - \(billinfo)")
        
        let cityStr = billinfo["city"] as! String
        
        let address2 = "\(billinfo["house_number"] as! String)" + "\(billinfo["address"] as! String)"
        
        let address1 = billinfo["company_name"] as! String
        
        let postalStr = billinfo["zip"] as! String
        
        let stateStr = billinfo["state"] as! String
        
        let addressDict = ["city":cityStr, "line1":address1,"line2":address2, "postal_code":postalStr,"state":stateStr]
        
        
        var addressDataDict = [String : Any]()
        
        addressDataDict = ["address":addressDict]
        print(addressDataDict)
   
       print("customerId - \(customerId)")
       print("getshippingId - \(getshippingId)")
        print("addresspassed - \(addressDataDict)")
       
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let orderidStrheader = String(orderresultorderid)
           
           let headers: HTTPHeaders = [
               "Content-Type": "application/json",
               "Authorization": autho,
               "order_id": orderidStrheader,
               "user_id": customeridStr,
               "cart_id": cartidStr,
               "action": "payment"
           ]

              let urlString = GlobalObjects.DevlopmentApi+"payment/"

       AF.request(urlString, method: .post, parameters: ["currency": "USD", "amount": orderresultamount,"receipt_email": customerEmailId,"type": "card","card": cardDataDict,"billing_details": addressDataDict,"metadata": metadataDict],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
              response in
                switch response.result {
                              case .success:
                                 // print(response)

                                  if response.response?.statusCode == 200{
                                   
                                   let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    print(dict)
                                    
//                                    let shippingMDict:NSDictionary = dict["shippingmethod"]as! NSDictionary
//
//                                    shippingmethodStr = shippingMDict["name"]as! String
                           //         paymentmethodStr = dict["payment_method"]as! String
                                  
                                    print("payment Resultshow - \(dict)")
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title:nil , message: "Order placed successfully",         preferredStyle: UIAlertController.Style.alert)

                                  
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    
                                        self.performSegue(withIdentifier: "afterpaymentDetail", sender: self)
                                                                    
                                        
                                                                    
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor(rgb: 0xFC4355)
                                   
                                    
                                   
                                  }else{
                                   
                                    ERProgressHud.sharedInstance.hide()

                                    if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()
                                        self.showSimpleAlert(messagess: "Incorrect card details. Please check")
                                        
                                    }else if response.response?.statusCode == 401{
                                       
                                        ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                       
                                   }else if response.response?.statusCode == 500{
                                       
                                    ERProgressHud.sharedInstance.hide()

                                       let dict :NSDictionary = response.value! as! NSDictionary
                                       
                                       self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                   }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                   
                               
                                   
                                  }
                                  
                                  break
                              case .failure(let error):
                                
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                              }
              }


           }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "afterpaymentDetail" {
            let view = segue.destination as! AfterpaymentPageViewController
            view.orderIDGet = self.orderresultorderid
//            view.shippingmethodStrpass = self.shippingmethodStr
//            view.paymentmethodStrpass = self.paymentmethodStr
        }
    }
   
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: messagess, message: nil,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        ERProgressHud.sharedInstance.hide()
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFC4355)
    }
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        self.performSegue(withIdentifier: "backlogin", sender: self)
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFC4355)
    }
    
    
    
    @IBAction func changeBtnClicked(_ sender: Any) {
        
        self.viewshow()
        
    }
    
    
    @IBAction func closechangeClicked(_ sender: Any) {
        
        otherTf.resignFirstResponder()
        self.viewhide()
        
    }
    
    
    func viewshow()  {
        
        closeBTn.layer.cornerRadius = 6
        closeBTn.layer.borderWidth = 1
        closeBTn.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
        
        changeSubmitBtn.layer.cornerRadius = 6
        changeViewBase.layer.cornerRadius = 6
        otherTfouterView.layer.cornerRadius = 6
        
        blurView.isHidden = false
        changeViewBase.isHidden = false
        title1Lbl.isHidden = false
        title2Lbl.isHidden = false
        per5Lbl.isHidden = false
        per10Lbl.isHidden = false
        rup20Lbl.isHidden = false
        rup50Lbl.isHidden = false
        otherLbl.isHidden = false
        otherTfouterView.isHidden = false
        otherTf.isHidden = false
        redio5perBtn.isHidden = false
        redio10perBtn.isHidden = false
        redio20RupeeBtn.isHidden = false
        redio50RupeeBtn.isHidden = false
        otherBtn.isHidden = false
        changeSubmitBtn.isHidden = false
        closeBTn.isHidden = false
        otherclick = "no"
        redioclicked = "no"
        
        let imagehome = UIImage(named: "untickredio.png") as UIImage?
        self.redio5perBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio10perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.otherBtn.setBackgroundImage(image4, for: .normal)
        
        parametername = "tip"
        tipvalue = "0"
        
    }
    
    func viewhide()  {
        
        blurView.isHidden = true
        changeViewBase.isHidden = true
        title1Lbl.isHidden = true
        title2Lbl.isHidden = true
        per5Lbl.isHidden = true
        per10Lbl.isHidden = true
        rup20Lbl.isHidden = true
        rup50Lbl.isHidden = true
        otherLbl.isHidden = true
        otherTfouterView.isHidden = true
        otherTf.isHidden = true
        redio5perBtn.isHidden = true
        redio10perBtn.isHidden = true
        redio20RupeeBtn.isHidden = true
        redio50RupeeBtn.isHidden = true
        otherBtn.isHidden = true
        changeSubmitBtn.isHidden = true
        closeBTn.isHidden = true
    }
    
    
    @IBAction func per5Btnclicked(_ sender: Any) {
        parametername = "tip"
        tipvalue = "5"
        
        let imagehome = UIImage(named: "dot.jpg") as UIImage?
        self.redio5perBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio10perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.otherBtn.setBackgroundImage(image4, for: .normal)
        
        redioclicked = "yes"
        otherTf.resignFirstResponder()
    }
    
    @IBAction func per10BtnClicked(_ sender: Any) {
        parametername = "tip"
        tipvalue = "10"
        
        let imagehome = UIImage(named: "dot.jpg") as UIImage?
        self.redio10perBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio5perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.otherBtn.setBackgroundImage(image4, for: .normal)
        
        redioclicked = "yes"
        otherTf.resignFirstResponder()
    }
    
    @IBAction func rupee20BtnClicked(_ sender: Any) {
//        parametername = "custom_tip"
//        tipvalue = "4"
        
        parametername = "tip"
        tipvalue = "15"
        
        let imagehome = UIImage(named: "dot.jpg") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio5perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio10perBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.otherBtn.setBackgroundImage(image4, for: .normal)
        
        redioclicked = "yes"
        otherTf.resignFirstResponder()
    }
    
    @IBAction func rupee50BtnClicked(_ sender: Any) {
//        parametername = "custom_tip"
//        tipvalue = "6"
        
        parametername = "tip"
        tipvalue = "20"
        
        let imagehome = UIImage(named: "dot.jpg") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio5perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio10perBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.otherBtn.setBackgroundImage(image4, for: .normal)
        
        redioclicked = "yes"
        otherTf.resignFirstResponder()
    }
    
    @IBAction func otheramtBtnClicked(_ sender: Any) {
        parametername = "custom_tip"
        otherclick = "yes"
        
        let imagehome = UIImage(named: "dot.jpg") as UIImage?
        self.otherBtn.setBackgroundImage(imagehome, for: .normal)
        
        let image1 = UIImage(named: "untickredio.png") as UIImage?
        self.redio5perBtn.setBackgroundImage(image1, for: .normal)
        
        let image2 = UIImage(named: "untickredio.png") as UIImage?
        self.redio10perBtn.setBackgroundImage(image2, for: .normal)
        
        let image3 = UIImage(named: "untickredio.png") as UIImage?
        self.redio20RupeeBtn.setBackgroundImage(image3, for: .normal)
        
        let image4 = UIImage(named: "untickredio.png") as UIImage?
        self.redio50RupeeBtn.setBackgroundImage(image4, for: .normal)
        
        redioclicked = "yes"
        otherTf.isUserInteractionEnabled = true

    }
    
    @IBAction func changesubmitClicked(_ sender: Any) {
        
        otherTf.resignFirstResponder()
        
        if redioclicked == "yes" {
         
        if otherclick == "yes" {
            
            if otherTf.text == "" || otherTf.text == nil {
                
                showSimpleAlert(messagess: "Please enter other amount")
            }else{
            tipvalue = otherTf.text!
                viewhide()
                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                fisrttimefeeapi()
            }
            
        }else{
           
            viewhide()
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
            fisrttimefeeapi()
            
        }
        
       
            
        }else{
           
            showSimpleAlert(messagess: "Select atlest one tip option")

            
            }
        
    }
    


    
    func pickershow()  {
        
        blurView.isHidden = false
        datepickerouterView.isHidden = false
         piockergreenview.isHidden = false
         greenselectrLbl.isHidden = false
         pickerinnerView.isHidden = false
        pickercancelBtn.isHidden = false
        pickerOkBtn.isHidden = false
        
        if whichbtn == "yr" {
            pickerDateView.isHidden = true
            pickerinnerView.isHidden = true
            yearpicker.isHidden = false
            greenselectrLbl.text = "Select Year"
        }else{
            pickerDateView.isHidden = false
            pickerinnerView.isHidden = true
            yearpicker.isHidden = true
            greenselectrLbl.text = "Select Month"

        }
    }
    
    func pickerhide()  {
        
        blurView.isHidden = true
        datepickerouterView.isHidden = true
         piockergreenview.isHidden = true
         greenselectrLbl.isHidden = true
         pickerinnerView.isHidden = true
        pickercancelBtn.isHidden = true
        pickerOkBtn.isHidden = true
        pickerDateView.isHidden = true

        
    }
    
    
    @IBAction func monthBtnClicked(_ sender: Any) {

      whichbtn = "mon"
        pickershow()
        
    }
    
    @IBAction func yearBtnClicked(_ sender: Any) {
        
        whichbtn = "yr"

        pickershow()
        
    }
    
//    @objc func dateChanged(_ picker: MonthYearPickerView) {
//        print("date changed: \(picker.date)")
//
////        let dateFormatter = DateFormatter()
////            dateFormatter.dateFormat = "MM"
////        let dateString = dateFormatter.string(from: picker.date)
////        print("month \(dateString)")
////
////        stingOFCardMonth = dateString
////        monthBtn.setTitle(dateString, for: UIControl.State.normal)
//
//
//        let dateFormatter1 = DateFormatter()
//        dateFormatter1.dateFormat = "YYYY"
//        let dateString1 = dateFormatter1.string(from: picker.date)
//        print("year \(dateString1)")
//
//        stringOfCardYear = dateString1
//        yearBtn.setTitle(dateString1, for: UIControl.State.normal)
//
//    }
    
    @IBAction func pickerOkBtnClicked(_ sender: Any) {
    
        if stingOFCardMonth == "" || stringOfCardYear == "" {
            
            
        }else{
        
        
        let dateappend = stingOFCardMonth + "/" + stringOfCardYear
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/yyyy"
        let enteredDate = dateFormatter.date(from: dateappend)!
        let endOfMonth = Calendar.current.date(byAdding: .month, value: 1, to: enteredDate)!
        let now = Date()
        if (endOfMonth < now) {
            
            showSimpleAlert(messagess: "Expired month")
            
        } else {
            // valid
            print("valid - now: \(now) entered: \(enteredDate)")
        }
        
        }
        
        pickerhide()
    }
    
    @IBAction func pickerCancelClicked(_ sender: Any) {
        
        pickerhide()
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if (text == "\n") {
                textbaseViewTf.resignFirstResponder()
            }
            return true
        }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Tell the keyboard where to go on next / go button.
       
        textField.resignFirstResponder()
        

        return true
    }
    
    func setupToolbar(){
            //Create a toolbar
            let bar = UIToolbar()
            
            //Create a done button with an action to trigger our function to dismiss the keyboard
        let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissMyKeyboard))
            
            //Create a felxible space item so that we can add it around in toolbar to position our done button
            let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            
            //Add the created button items in the toobar
            bar.items = [flexSpace, flexSpace, doneBtn]
            bar.sizeToFit()
            
            //Add the toolbar to our textfield
            otherTf.inputAccessoryView = bar
        creditcardnumberTF.inputAccessoryView = bar
        securityTF.inputAccessoryView = bar
            
        }
        
        @objc func dismissMyKeyboard(){
            view.endEditing(true)
        }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
}
