//
//  OrederpageVC.swift
//  Restaurant
//
//  Created by TISSA Technology on 11/4/20.
//

import UIKit
import Alamofire
import SDWebImage

class OrederpageVC: UIViewController,UITableViewDataSource, UITableViewDelegate  {
    
    @IBOutlet weak var orderTable: UITableView!

    var orderlistaraay = NSArray()
    var orderclickid = String()
    var orderresultorderid = Int()
    var currencygetandpass = String()
    var shippingmethodStr = String()
    var paymentmethodStr = String()
    var getstorenameStr = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        GlobalObjects.pageback = "yes"
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        getorderlist()
        
        orderTable.register(UINib(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier:"Cell")
     
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.view.alpha = 0.7
        UIView.animate(withDuration: 1.5, animations: {
                self.view.alpha = 1.0
            })
    }
    
    
    func getorderlist()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        let admintoken = defaults.object(forKey: "custToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalObjects.DevlopmentApi+"customer-payment/?customer_id=\(customerId)&restaurant_id=\(GlobalObjects.restaurantGlobalid)"
         
        
        print("oderitems get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "action": "customer-payment"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    self.orderlistaraay  = response.value! as! NSArray
                                     
                                    print( self.orderlistaraay)
                                    
                                    if self.orderlistaraay.count == 0 {
                                       
                                        ERProgressHud.sharedInstance.hide()
                                        self.showSimpleAlert(messagess:"No order available")
                                        
                                        
                                        
                                    }else{
                                        
                                        
                                        orderTable.reloadData()
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        
                                    }
                                   
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                        self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                           }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFC4355)
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        self.performSegue(withIdentifier: "backlogin", sender: self)
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFC4355)
    }
    
    
    
    
    
    //MARK: - Table View Delegates And Datasource
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderlistaraay.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! OrderCell

        
        cell.itemouterview.layer.cornerRadius = 6
        cell.itemouterview.layer.shadowColor = UIColor.lightGray.cgColor
        cell.itemouterview.layer.shadowOpacity = 1
        cell.itemouterview.layer.shadowOffset = .zero
        cell.itemouterview.layer.shadowRadius = 3
        
         let dictObj = self.orderlistaraay[indexPath.row] as! NSDictionary
         
         let storedata:NSDictionary = dictObj["order"]as! NSDictionary
         
         let orderno = storedata["order_id"] as! Int
        
        cell.ordernoLbl.text = String(orderno)
         
         let strdate = (dictObj["created_at"] as! String)
         
         
         let dateFormatter = DateFormatter()
                 dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
                 dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                 let date = dateFormatter.date(from: strdate)// create   date from string

                 // change to a readable time format and change to local time zone
        if date == nil {
         
            let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                    dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
                    let date1 = dateFormatter.date(from: strdate)
            dateFormatter.dateFormat = "MMMM dd,yyyy"
            dateFormatter.timeZone = NSTimeZone.local
            let timeStamp1 = dateFormatter.string(from: date1!)
    
    
    cell.orderdateLbl.text = timeStamp1
            
        }else{
        
                 dateFormatter.dateFormat = "MMMM dd,yyyy"
                 dateFormatter.timeZone = NSTimeZone.local
                 let timeStamp = dateFormatter.string(from: date!)
         
         
         cell.orderdateLbl.text = timeStamp
        }
    //    let country = storedata["currency"] as! String
        
        
        
//        if country == "INR" {
//
//                        let rupee = "\u{20B9}"
//
//                         let uprice = dictObj["amount"] as! String
//
//                        cell.totalLbl.text =  rupee + uprice
//
//                    }else{
                        
                    let rupee = "$"
                     
                        let priceLbl = dictObj["amount"]as! String
            
                    cell.totalLbl.text =  rupee + priceLbl
                     
                //    }
        
       
        
        let urlStr = dictObj["status"] as! String
    
         if urlStr == "CONFIRMED" {
             
             cell.statusLbl.text = "Confirmed"
//             cell.detailShowBtn.isHidden = false
//             cell.orderCancelBtn.isHidden = false
//             cell.fullviewdetailsBtn.isHidden = true
            
            cell.detailShowBtn.isHidden = true
            cell.orderCancelBtn.isHidden = true
            cell.fullviewdetailsBtn.isHidden = false
             
         }else if urlStr == "SUCCESS"{
             
             cell.statusLbl.text = "Success"
//             cell.detailShowBtn.isHidden = false
//             cell.orderCancelBtn.isHidden = false
//             cell.fullviewdetailsBtn.isHidden = true
            
            cell.detailShowBtn.isHidden = true
            cell.orderCancelBtn.isHidden = true
            cell.fullviewdetailsBtn.isHidden = false
             
         }else if urlStr == "FAIL"{
             
             cell.statusLbl.text = "Failed"
//             cell.detailShowBtn.isHidden = false
//             cell.orderCancelBtn.isHidden = false
//             cell.fullviewdetailsBtn.isHidden = true
            
            cell.detailShowBtn.isHidden = true
            cell.orderCancelBtn.isHidden = true
            cell.fullviewdetailsBtn.isHidden = false
             
         }else if urlStr == "READY_TO_FULFILL"{
             
             cell.statusLbl.text = "Ready to fulfill"
             cell.detailShowBtn.isHidden = true
             cell.orderCancelBtn.isHidden = true
             cell.fullviewdetailsBtn.isHidden = false
             
         }else if urlStr == "READY_TO_PICK"{
             
             cell.statusLbl.text = "Ready to pick"
             cell.detailShowBtn.isHidden = true
             cell.orderCancelBtn.isHidden = true
             cell.fullviewdetailsBtn.isHidden = false
             
         }else if urlStr == "PICKED"{
             
             cell.statusLbl.text = "Picked"
             cell.detailShowBtn.isHidden = true
             cell.orderCancelBtn.isHidden = true
             cell.fullviewdetailsBtn.isHidden = false
             
         }else if urlStr == "COMPLETE"{
             
             cell.statusLbl.text = "Completed"
             cell.detailShowBtn.isHidden = true
             cell.orderCancelBtn.isHidden = true
             cell.fullviewdetailsBtn.isHidden = false
             
         }else if urlStr == "CANCELED"{
             
             cell.statusLbl.text = "Cancelled"
             cell.detailShowBtn.isHidden = true
             cell.orderCancelBtn.isHidden = true
             cell.fullviewdetailsBtn.isHidden = false
             
         }
       
         
         cell.paymentLbl.text = (dictObj["payment_method"] as! String)
        
         cell.detailShowBtn.layer.cornerRadius = 8
         cell.orderCancelBtn.layer.cornerRadius = 8
         cell.fullviewdetailsBtn.layer.cornerRadius = 8
         
         cell.detailShowBtn.tag = indexPath.row
         cell.detailShowBtn.addTarget(self, action: #selector(detailShowBtnClicked(_:)), for: .touchUpInside)
         
         cell.orderCancelBtn.tag = indexPath.row
         cell.orderCancelBtn.addTarget(self, action: #selector(orderCancelBtnClicked(_:)), for: .touchUpInside)
         
         cell.fullviewdetailsBtn.tag = indexPath.row
         cell.fullviewdetailsBtn.addTarget(self, action: #selector(fullviewdetailsBtnClicked(_:)), for: .touchUpInside)
         
       
        

        return cell
    }
   
    
    
    
    
    @IBAction func orderCancelBtnClicked(_ sender: UIButton) {
    
        let index = sender.tag
       
        let dictObj = self.orderlistaraay[index] as! NSDictionary
        
        let storedata:NSDictionary = dictObj["order"]as! NSDictionary
        
        let orderno = storedata["order_id"] as! Int
        
        orderclickid = String(orderno)
        
       
        let alert = UIAlertController(title: nil, message: "Are you sure you want to cancel?",         preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action//
        }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                         self.deleteorderapi()
                                         
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFC4355)
        
        
    
    }
    
    func deleteorderapi()  {
        
        let defaults = UserDefaults.standard
        
      //  let admintoken = defaults.object(forKey: "adminToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        let admintoken = defaults.object(forKey: "custToken")as? String
           
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
      //  let trimmedString = categoryStr.removingAllWhitespaces()
        
        let urlString = GlobalObjects.DevlopmentApi+"payment/\(orderclickid)"
        
       

            
        print("delete get url - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "order_id": orderclickid,
                "user_id": customeridStr,
                "action": "payment"
            ]

        AF.request(urlString, method: .delete, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                
                                   
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title: "Order cencel successfully", message: nil,         preferredStyle: UIAlertController.Style.alert)

                                  
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    //Sign out action
                                                                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                                                                getorderlist()
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor(rgb: 0x199A48)
                                   
                    
                                 
                                }else{
                                    
                        if response.response?.statusCode == 401{
                                    
                            ERProgressHud.sharedInstance.hide()
                        self.SessionAlert()
                          
                           }else if response.response?.statusCode == 500{
                                        
                            ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                            
                           }else if response.response?.statusCode == 400{
                            
                            ERProgressHud.sharedInstance.hide()

                            let dict :NSDictionary = response.value! as! NSDictionary
                            
                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
               }else{
                                        
                      self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    
    
    
    
    @IBAction func fullviewdetailsBtnClicked(_ sender: UIButton) {
    
        let index = sender.tag
       
        let dictObj = self.orderlistaraay[index] as! NSDictionary
        
        let storedata:NSDictionary = dictObj["order"]as! NSDictionary
        
        let storenamedetail:NSDictionary = dictObj["restaurant"]as! NSDictionary
        getstorenameStr = storenamedetail["name"]as! String
        
        self.orderresultorderid = storedata["order_id"] as! Int
        
        if dictObj["shippingmethod"] is NSNull {
            shippingmethodStr = "Restaurant Pickup"
        }else{
        
        let shippingMDict:NSDictionary = dictObj["shippingmethod"]as! NSDictionary
        
        shippingmethodStr = shippingMDict["name"]as! String
            
        }
        paymentmethodStr = dictObj["payment_method"]as! String
        
        let defaults = UserDefaults.standard
        defaults.set(storedata, forKey: "passOrderResultfromlist")
                
        self.performSegue(withIdentifier: "orderdetail", sender: self)
    
    }
    
    
    @IBAction func detailShowBtnClicked(_ sender: UIButton) {
        
        let index = sender.tag
       
        let dictObj = self.orderlistaraay[index] as! NSDictionary
        
        let storedata:NSDictionary = dictObj["order"]as! NSDictionary
        
        let storenamedetail:NSDictionary = dictObj["restaurant"]as! NSDictionary
        getstorenameStr = storenamedetail["name"]as! String
        
        self.orderresultorderid = storedata["order_id"] as! Int
      
        if dictObj["shippingmethod"] is NSNull {
            shippingmethodStr = "Restaurant Pickup"
        }else{
        
        let shippingMDict:NSDictionary = dictObj["shippingmethod"]as! NSDictionary
        
        shippingmethodStr = shippingMDict["name"]as! String
            
        }
        paymentmethodStr = dictObj["payment_method"]as! String
        
        let defaults = UserDefaults.standard
        defaults.set(storedata, forKey: "passOrderResultfromlist")
    
        self.performSegue(withIdentifier: "orderdetail", sender: self)
    
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "orderdetail" {
            
        let view = segue.destination as! OrderDetailPage
        view.orderIDGet = self.orderresultorderid
      //  view.currencyPassed = self.currencygetandpass
        view.shippingmethodStrpass = self.shippingmethodStr
        view.paymentmethodStrpass = self.paymentmethodStr
        view.storenamepassStr = self.getstorenameStr
            
        }
        
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return 179
        
    }
    
    

    @IBAction func orderbackBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)

       
    }

}
