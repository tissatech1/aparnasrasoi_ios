//
//  CartCell.swift
//  Restaurant
//
//  Created by TISSA Technology on 11/4/20.
//

import UIKit

class CartCell: UITableViewCell {

    @IBOutlet var cartProductImg: UIImageView!
    @IBOutlet var cartProName: UILabel!
    @IBOutlet var quantityLbl: UILabel!
    @IBOutlet var costLbl: UILabel!
    @IBOutlet var deletcartBtn: UIButton!
    @IBOutlet var tableCellOuter: UIView!
    @IBOutlet weak var ingredienttxtLbl: UITextView!
    @IBOutlet weak var ingredientTitle: UILabel!
    @IBOutlet weak var counterview: UIView!
    @IBOutlet weak var QtyminusBtn: UIButton!
    @IBOutlet weak var QtyPlusBtn: UIButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
