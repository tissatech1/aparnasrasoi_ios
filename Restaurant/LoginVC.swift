//
//  LoginVC.swift
//  Restaurant
//
//  Created by TISSA Technology on 10/31/20.
//

import UIKit
import Alamofire
import IQKeyboardManagerSwift


class LoginVC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var guestmobilenumberview: UIView!
    
    @IBOutlet weak var guestcountrycodeTF: UITextField!
    @IBOutlet weak var guestmobileTF: UITextField!
    
    @IBOutlet weak var BlurView: UIView!
    @IBOutlet weak var otpView: UIView!
    @IBOutlet weak var OtpTF: UITextField!
    @IBOutlet weak var backlogBtn: UIButton!

    
    
    var forgotusername = String()
    var forgotpassword = String()
    var guestmobile = String()
    var verifyCode = String()
    var getid = Int()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        IQKeyboardManager.shared.enable = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        guestmobilenumberview.isHidden = true
        BlurView.isHidden = true
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            backlogBtn.isHidden = false
            backlogBtn.layer.cornerRadius = 8
        }else{
            
            backlogBtn.isHidden = true
        }

    }
    
    @IBAction func backlogBtnClicked(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "DashboardVC", sender: self)
   
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.view.alpha = 0.7
        UIView.animate(withDuration: 1.5, animations: {
                self.view.alpha = 1.0
            })
    }
    
    @IBAction func logBtnClicked(_ sender: UIButton) {
        
        if usernameTF.text == nil || usernameTF.text == "UserName" || usernameTF.text == "" || usernameTF.text == " "{
            self.showSimpleAlert(messagess: "Enter username")
        }else if passwordTF.text == nil || passwordTF.text == "Password" || passwordTF.text == "" || passwordTF.text == " "{
            self.showSimpleAlert(messagess: "Enter password")
        }else{
            
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")

            loginApi()
            
        }
        
        
    }
    
    @IBAction func SignupBtnClicked(_ sender: UIButton) {
        

        self.performSegue(withIdentifier: "signup", sender: self)
        
        
    }
    
    
     func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "signupVC" {
            
        }else{
            
        }
       
    }


    
    //MARK: Webservice Call

    
   func loginApi() {


       let urlString = GlobalObjects.DevlopmentApi+"rest-auth/login/v1/"

    AF.request(urlString, method: .post, parameters: ["username": usernameTF.text as Any, "password": passwordTF.text as Any,"restaurant_id":GlobalObjects.restaurantGlobalid],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                               let status = dict.value(forKey: "id")
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(status, forKey: "custId")
                            defaults.set(tok, forKey: "custToken")
                            
                            defaults.set("loged", forKey: "Usertype")
                            
                            defaults.set("reguser", forKey: "Userlog")
                            GlobalObjects.customertoken = tok as! String
                            
                            ERProgressHud.sharedInstance.hide()

                            
                                
                            self.performSegue(withIdentifier: "DashboardVC", sender: self)

                            
                            
                           }else{
                            
                             if response.response?.statusCode == 403{
                                
                                ERProgressHud.sharedInstance.hide()

                                let dict :NSDictionary = response.value! as! NSDictionary
                                
                               let msgtxt = dict.value(forKey: "msg") as! String
                                
                                if msgtxt == "You do not have access to restaurant." {
                                   
                                    let alert = UIAlertController(title: nil, message: "You do not have access to this restaurant. You want to login for this restaurant please click on OK.",         preferredStyle: UIAlertController.Style.alert)

                                    alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
                                        //Cancel Action//
                                    }))
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                                                    //Sign out action
                                                                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                                                    self.tokenapi()
                                                                     
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor.black
                                    
                                    
                                    
                                }else{
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    
                                }
                                
                            }else if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()

                                self.showSimpleAlert(messagess: "Username or password is incorrect")
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                            

                           }
                           
                           break
                       case .failure(let error):
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No Internet Detected")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                        }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                       }

                           print(error)
                       }
       }


    }
    
    
    func Getidapi(){
        
       
        let defaults = UserDefaults.standard
        
        let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
        let urlString = GlobalObjects.DevlopmentApi+"/user/?username=\(usernameTF.text!)"
        
           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                 
                                    let dict : NSDictionary = response.value! as! NSDictionary
                                    
                                    let firstarr : NSArray = dict["results"]as! NSArray
                                    
                                    let firstdict : NSDictionary = firstarr[0]as! NSDictionary
                                    
                                    let firstid = firstdict.value(forKey: "id")
                                    
                                    print(firstid ?? 00)

                                    self.getid = firstid as! Int
                                    
                                    self.addidrestaurant()
                                    
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()
                  }
                     if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                   
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
  
    func tokenapi(){

        let urlString = GlobalObjects.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalObjects.adminusername, "password":GlobalObjects.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            
                        //    self.signupApi()
                          
                            self.Getidapi()
                            
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
   
    func addidrestaurant() {

        let defaults = UserDefaults.standard
        
        let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
            
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                
            ]

        let urlString = GlobalObjects.DevlopmentApi+"userrestaurant/"

        AF.request(urlString, method: .post, parameters: ["user":getid ,"restaurant":GlobalObjects.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                             
                             ERProgressHud.sharedInstance.hide()

                                ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                self.loginApi()
                             
                            }else{
                             
                               if response.response?.statusCode == 403{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                let msgtxt = dict.value(forKey: "msg") as! String
                                 
                                 if msgtxt == "You do not have access to restaurant." {
                                    
                                     let alert = UIAlertController(title: nil, message: "You do not have access to restaurant.You want to login for this restaurant please click on OK.",         preferredStyle: UIAlertController.Style.alert)

                                     alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
                                         //Cancel Action//
                                     }))
                                     alert.addAction(UIAlertAction(title: "OK",
                                                                   style: UIAlertAction.Style.default,
                                                                   handler: {(_: UIAlertAction!) in
                                                                     //Sign out action
                                                                     ERProgressHud.sharedInstance.show(withTitle: "Loading...")

                                                                     self.tokenapi()
                                                                      
                                     }))
                                     self.present(alert, animated: true, completion: nil)
                                     alert.view.tintColor = UIColor(rgb: 0xFE9300)
                                     
                                     
                                     
                                 }else{
                                 
                                 self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                     
                                 }
                                 
                             }else if response.response?.statusCode == 401{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 self.showSimpleAlert(messagess: "Username or password is incorrect")
                             }else{
                                 
                                 self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                }
                             

                            }
                            
                            break
                        case .failure(let error):
                         ERProgressHud.sharedInstance.hide()
                         print(error.localizedDescription)
                         
                         let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                         
                         
                         let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                         
                         let msgrs = "URLSessionTask failed with error: The request timed out."
                         
                         if error.localizedDescription == msg {

                             self.showSimpleAlert(messagess:"No Internet Detected")

                         }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                             self.showSimpleAlert(messagess:"Slow Internet Detected")

                         }else{
                         
                             self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                            print(error)
                        }
        }


     }
     
     
    
    
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFC4355)
    }
    
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn _: NSRange, replacementString string: String) -> Bool {
//
//        if textField == usernameTF {
//                    let validString = CharacterSet(charactersIn: " !@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢")
//
//                    if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
//                        return false
//                    }
//                    if let range = string.rangeOfCharacter(from: validString)
//                    {
//                        print(range)
//                        return false
//                    }
//                }
//
//        return true
//
//    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Tell the keyboard where to go on next / go button.
       
        textField.resignFirstResponder()
        

        return true
    }
    
   
    @IBAction func forgotusernameClicked(_ sender: UIButton) {

        var answer = String()
        
        let ac = UIAlertController(title: "Username  Reset", message: nil, preferredStyle: .alert)
       
        ac.addTextField { (textField) in
            textField.placeholder = "Enter your email"
            textField.textAlignment = .center
        }
        
        let submitAction = UIAlertAction(title: "OK", style: .default) { [self, unowned ac] _ in
                 let textstr = ac.textFields![0]
                answer = textstr.text!
                
                if answer == ""{
                    
                    self.showSimpleAlert(messagess: "Enter email")
                }else{
                    
                    forgotusername = answer
                    
                    ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                    usenamereset()
                    
                }
                
            }
        
        let CANCELAction = UIAlertAction(title: "CANCEL", style: .default) {  _ in
            
        }

            ac.addAction(CANCELAction)
            ac.addAction(submitAction)

            present(ac, animated: true)
        ac.view.tintColor = UIColor.black
        
    }
    
    
    @IBAction func forgotpasswordclicked(_ sender: UIButton) {
        
        var answer = String()
 
        let ac = UIAlertController(title: "Password  Reset", message: nil, preferredStyle: .alert)
       
        ac.addTextField { (textField) in
            textField.placeholder = "Enter your username"
            textField.textAlignment = .center
        }
        
            let submitAction = UIAlertAction(title: "OK", style: .default) { [unowned ac] _ in
                let textstr = ac.textFields![0]
               answer = textstr.text!
               
               if answer == ""{
                   
                   self.showSimpleAlert(messagess: "Enter username")
               }else{
                
                self.forgotpassword = answer
                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                self.forgotpassApi()
                   
               }
                
                
            }
        
        let CANCELAction = UIAlertAction(title: "CANCEL", style: .default) {  _ in
            
        }

            ac.addAction(CANCELAction)
            ac.addAction(submitAction)

            present(ac, animated: true)
        ac.view.tintColor = UIColor.black
        
    }
    
    
    func usenamereset()  {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
       // let customerId = defaults.integer(forKey: "custId")
        
      
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalObjects.DevlopmentApi + "forgot/user/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .post, parameters: ["email":forgotusername],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title:nil , message: "Username reset email has been sent",         preferredStyle: UIAlertController.Style.alert)


                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in



                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor(rgb: 0x199A48)
                                    
                                 
                                }else{
                                    
                                     if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func forgotpassApi() {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
       // let customerId = defaults.integer(forKey: "custId")
        
      
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalObjects.DevlopmentApi + "rest-auth/password/reset/v1/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .post, parameters: ["username":forgotpassword],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    ERProgressHud.sharedInstance.hide()

                                    let alert = UIAlertController(title:nil , message: "Password reset email has been sent",         preferredStyle: UIAlertController.Style.alert)


                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in



                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor(rgb: 0x199A48)
                                    
                                 
                                }else{
                                    
                                     if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    @IBAction func guestloginviewCancleClicked(_ sender: Any) {
        
        
            
//            UIView.animate(withDuration: 0.5, delay: 0.2, options: UIView.AnimationOptions.curveEaseOut, animations: {
//                self.guestmobilenumberview.alpha = 0
//            }, completion: { finished in
//                self.guestmobilenumberview.isHidden = true
//            })
        
        UIView .transition(with: self.guestmobilenumberview, duration: 0.5, options: .transitionCurlUp,
                                       animations: {
                    self.guestmobilenumberview.isHidden = true

                    })
        
        guestmobileTF.text = nil
        guestmobileTF.placeholder = "Mobile number"
        guestcountrycodeTF.text = "+1"
        
    }
    
    @IBAction func guestloginClicked(_ sender: Any) {
       
        
//        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.transitionCurlUp, animations: {
//
//            self.guestmobilenumberview.alpha = 1
//
//        }, completion: { finished in
//            self.guestmobilenumberview.isHidden = false
//        })

        UIView .transition(with: self.guestmobilenumberview, duration: 0.5, options: .transitionCurlDown,
                                       animations: {
                    self.guestmobilenumberview.isHidden = false

                    })
                
        guestcountrycodeTF.resignFirstResponder()
       
    }
    
    
    @IBAction func guestGetOtpClicked(_ sender: Any) {
        
        if guestcountrycodeTF.text == nil || guestcountrycodeTF.text == "" || guestcountrycodeTF.text == " " {
            
            showSimpleAlert(messagess: "Enter country code")
        }else if guestmobileTF.text == nil || guestmobileTF.text == "" || guestmobileTF.text == " " {
            
            showSimpleAlert(messagess: "Enter mobile number")
        }else{
        
        guestmobile = guestcountrycodeTF.text!
            + guestmobileTF.text!
        
         ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GestUserApi()
            
        }
    }
    
    
    
    func GestUserApi() {


        let urlString = GlobalObjects.DevlopmentApi+"send/code/"

         AF.request(urlString, method: .post, parameters: ["phone_number": guestmobile],encoding: JSONEncoding.default, headers: nil).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                       
                                ERProgressHud.sharedInstance.hide()

                                self.BlurView.isHidden = false

//                                var answer = String()
//
//                                let ac = UIAlertController(title: "Verify Code", message: nil, preferredStyle: .alert)
//
//                                ac.addTextField { (textField) in
//                                    textField.placeholder = "Enter verification code"
//                                    textField.textAlignment = .center
//                                    //textField.keyboardType = .phonePad
//                                    textField.resignFirstResponder()
//                                }
//
//                                let submitAction = UIAlertAction(title: "OK", style: .default) { [self, unowned ac] _ in
//                                         let textstr = ac.textFields![0]
//                                        answer = textstr.text!
//
//                                        if answer == ""{
//
//                                            self.showSimpleAlert(messagess: "Enter 4 digit code")
//                                        }else{
//
//                                            verifyCode = answer
//
//                                            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
//
//                                            UserVerifyApi()
//
//                                        }
//
//                                    }
//
//                                let CANCELAction = UIAlertAction(title: "CANCEL", style: .default) {  _ in
//
//                                }
//
//                                    ac.addAction(CANCELAction)
//                                    ac.addAction(submitAction)
//
//                                self.present(ac, animated: true)
//                                    ac.view.tintColor = UIColor(rgb: 0xFE9300)
                                
                             
                            }else{
                             
                              if response.response?.statusCode == 403{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                 self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                             }else if response.response?.statusCode == 401{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 self.showSimpleAlert(messagess: "Username or password is incorrect")
                                
                             }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                self.showSimpleAlert(messagess: "\(self.guestmobile) is not a valid phone number.")
                               
                            }else{
                                 
                                 self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                }
                             

                            }
                            
                            break
                        case .failure(let error):
                         ERProgressHud.sharedInstance.hide()
                         print(error.localizedDescription)
                         
                         let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                         
                         
                         let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                         
                         let msgrs = "URLSessionTask failed with error: The request timed out."
                         
                         if error.localizedDescription == msg {
                             
                             self.showSimpleAlert(messagess:"No Internet Detected")
                             
                         }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                             
                             self.showSimpleAlert(messagess:"Slow Internet Detected")
                             
                         }else{
                         
                             self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                         }

                            print(error)
                        }
        }


     }
     
    
    @IBAction func OtpSubmitClicked(_ sender: Any) {
        
        if OtpTF.text == nil || OtpTF.text == "" || OtpTF.text == " "{
            
            showSimpleAlert(messagess: "Enter OTP")
        }else{
        
        verifyCode = OtpTF.text!
       
         ERProgressHud.sharedInstance.show(withTitle: "Loading...")
       
            adminlogincheck()
        }
        
    }
    
    
    
    @IBAction func otpcancelClicked(_ sender: Any) {
        
        self.BlurView.isHidden = true
        
        OtpTF.text = nil

    }
    
    func adminlogincheck(){

        let urlString = GlobalObjects.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalObjects.adminusername, "password":GlobalObjects.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "custToken")
                            
                            self.UserVerifyApi()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    func UserVerifyApi() {


        let urlString = GlobalObjects.DevlopmentApi+"guest/verify/"

        AF.request(urlString, method: .post, parameters: ["phone_number": guestmobile,"verification_code":verifyCode],encoding: JSONEncoding.default, headers: nil).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                       
                                 
                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 // print(dict)
                                    let status = dict.value(forKey: "id")
                                 let tok = dict.value(forKey: "token")
                                 
                                 let defaults = UserDefaults.standard
                                 
                                 defaults.set(status, forKey: "custId")
                                 defaults.set(tok, forKey: "custToken")
                                 
                                 defaults.set("loged", forKey: "Usertype")
                                defaults.set("guestuser", forKey: "Userlog")
                                 GlobalObjects.customertoken = tok as! String
                                 
                                 ERProgressHud.sharedInstance.hide()
   
                                 self.performSegue(withIdentifier: "DashboardVC", sender: self)

                            }else{
                             
                              if response.response?.statusCode == 403{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                 self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                             }else if response.response?.statusCode == 401{
                                 
                                 ERProgressHud.sharedInstance.hide()

                                 self.showSimpleAlert(messagess: "Username or password is incorrect")
                             }else if response.response?.statusCode == 400{
                                
                                ERProgressHud.sharedInstance.hide()

                                let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                            }else{
                                 
                                 self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                }
                             

                            }
                            
                            break
                        case .failure(let error):
                         ERProgressHud.sharedInstance.hide()
                         print(error.localizedDescription)
                         
                         let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                         
                         
                         let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                         
                         let msgrs = "URLSessionTask failed with error: The request timed out."
                         
                         if error.localizedDescription == msg {
                             
                             self.showSimpleAlert(messagess:"No Internet Detected")
                             
                         }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                             
                             self.showSimpleAlert(messagess:"Slow Internet Detected")
                             
                         }else{
                         
                             self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                         }

                            print(error)
                        }
        }


     }
    
    
    
   
}
extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}
