//
//  MenuDetailPage.swift
//  Restaurant
//
//  Created by TISSA Technology on 11/20/20.
//

import UIKit
import Alamofire
import SDWebImage
import ValueStepper



class MenuDetailPage: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var productImage: UIImageView!
    @IBOutlet var productName: UILabel!
    @IBOutlet var productUnitPrice: UILabel!
    @IBOutlet var productSubtotalPrice: UILabel!
    @IBOutlet var addToCartBtn: UIButton!
    @IBOutlet weak var ingredientstable: UITableView!
    @IBOutlet weak var ingredientlbl: UILabel!
    @IBOutlet weak var ingredientTableheight: NSLayoutConstraint!
    @IBOutlet weak var stepper1: ValueStepper!
    @IBOutlet weak var top1space: NSLayoutConstraint!
    @IBOutlet weak var top2space: NSLayoutConstraint!
    @IBOutlet weak var top3space: NSLayoutConstraint!
    @IBOutlet weak var textbaseViewTf: UITextView!


    
    var passunitprice = String()
    var paasprodyctimage = String()
    var passcountryStr = String()
    var passproductid = Int()
    var passproductaName = String()
    let cellReuseIdentifier = "cell"
    let ingrediantid = String()
    var ingrediantArray = NSArray()
    var selectiontagArray: [String] = []
    var ingredientsQtyArr: [Int] = []
    var IngredientConvertedPrice: [Double] = []
    var quantityNumber = String()
    
    var productunitprice = Double()

    
  //  let quantArray = ["1","2","3","4","5"]

    var counts = [Int](repeating: 0, count: 10)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ingredientstable.register(UINib(nibName: "ingredientTVCellTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        
        setup()
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GetIngredientList()
        
     //   checkCartAvl()
        stepper1.enableManualEditing = true
        quantityNumber = "1"

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.view.alpha = 0.7
        UIView.animate(withDuration: 1.5, animations: {
                self.view.alpha = 1.0
            })
    }
    
    func setup() {
        

        addToCartBtn.layer.cornerRadius = 6
   
        let url = URL(string: paasprodyctimage )
        
          
          productImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
          productImage.sd_setImage(with: url) { (image, error, cache, urls) in
              if (error != nil) {
                  // Failed to load image
                self.productImage.image = UIImage(named: "noimage.png")
              } else {
                  // Successful in loading image
                self.productImage.image = image
              }
          }
        
        productunitprice = Double(passunitprice)!
        
        productName.text = passproductaName
       
        let rupee = "$"
        let uprice = passunitprice
        let pri = String(uprice)
        
        let pricedata = pri
        
        productUnitPrice.text = rupee + pricedata
        productSubtotalPrice.text = rupee + pricedata
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    // Hide the Navigation Bar
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
    
    @IBAction func addToCartClicked(_ sender: Any) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
           
            let alert = UIAlertController(title:nil, message: "Please login to continue. We can't let just anyone have this access to ordering!",         preferredStyle: UIAlertController.Style.alert)

            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { _ in
                
                
                
            }))
            alert.addAction(UIAlertAction(title: "SIGN IN",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            GlobalObjects.backlogTag = "menulog"
                                        self.performSegue(withIdentifier: "backlogin", sender: self)
                                            
            }))
            
            self.present(alert, animated: true, completion: nil)
            alert.view.tintColor = UIColor(rgb: 0xFC4355)
            
        }else{
            
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        addToCart()
       // checkCartAvl()
    
        }
    }
   
    
    @IBAction func probackBtnClicked(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
   
    
    //MARK: - Table View Delegates And Datasource
        
      // number of rows in table view
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           
                return ingrediantArray.count

        }
        
        // create a cell for each table view row
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ingredientTVCellTableViewCell
                
                cell.selectionStyle = .none

                
                let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
                
                cell.ingredientname.text!  = dictObj["ingredient_name"] as! String
                let rupee = "$ "
                
                    let pricedata = dictObj["price"]as! String
                    let conprice = String(pricedata)
                
                cell.ingredientPrice.text = rupee + conprice
                
            cell.ingredientview.layer.borderWidth = 0.5
            cell.ingredientview.layer.cornerRadius = 6
            cell.ingredientview.layer.borderColor = UIColor(rgb: 0xFC4355).cgColor
            
            cell.minusClicked.tag = indexPath.row
            cell.minusClicked.addTarget(self, action: #selector(MinusClicked(_:)), for: .touchUpInside)
            
            cell.plusClicked.tag = indexPath.row
            cell.plusClicked.addTarget(self, action: #selector(PlusClicked(_:)), for: .touchUpInside)
            
            
            cell.selectwholeingBtn.tag = indexPath.row
            cell.selectwholeingBtn.addTarget(self, action: #selector(selectwholeingBtnClicked(_:)), for: .touchUpInside)
            
            
                return cell
           
        }
    
    @IBAction func selectwholeingBtnClicked(_ sender: UIButton) {
    
        if selectiontagArray[sender.tag] == "NO"{

            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = ingredientstable.cellForRow(at: indexPath) as! ingredientTVCellTableViewCell

            let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
            let pricedata = dictObj["price"]as! String

            IngredientConvertedPrice[indexPath.row] = Double(pricedata)!
            
            let totalQty = IngredientConvertedPrice.reduce(0, +)

            print("product unit price = \(productunitprice)")

            let addedprice = productunitprice + totalQty

         //   productunitprice = addedprice

            let rupee = "$"

            productSubtotalPrice.text = rupee + String(format: "%.2f", addedprice)
            
          
            cell.selectwholeingBtn.setBackgroundImage(UIImage(named: "right.jpg"), for: UIControl.State.normal)

          //  cell.selctionimg.image = UIImage(named: "right.jpg")

            
            selectiontagArray[indexPath.row] = "YES"

            print("selectatgarray - \(selectiontagArray)")
            
            ingredientsQtyArr[indexPath.row] = 1
            counts[indexPath.row] += 1
            
        }else{

            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = ingredientstable.cellForRow(at: indexPath) as! ingredientTVCellTableViewCell
            
            
            let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
            let pricedata = dictObj["price"]as! String

            counts[indexPath.row] = 0
            IngredientConvertedPrice[indexPath.row] = 0.00
            ingredientsQtyArr[indexPath.row] = 0
            cell.ingredientCountLBL.text = "1"
            
            let totalQty = IngredientConvertedPrice.reduce(0, +)

            print("product unit price = \(productunitprice)")

            let addedprice = productunitprice + totalQty

        //    productunitprice = addedprice

            let rupee = "$"

            productSubtotalPrice.text = rupee + String(format: "%.2f", addedprice)

          //  cell.selctionimg.image = UIImage(named: "blank-square.png")
            cell.selectwholeingBtn.setBackgroundImage(UIImage(named: "blank-square.png"), for: UIControl.State.normal)
     
            selectiontagArray[indexPath.row] = "NO"
            print("selectatgarray - \(selectiontagArray)")

            cell.ingredientPrice.text = rupee + pricedata
            
            
        }

        
    }
    
    
    
    @IBAction func MinusClicked(_ sender: UIButton) {
        
        
        
        if selectiontagArray[sender.tag] == "NO"{
           
           showSimpleAlert(messagess: "Please select ingredient first")
            
        }else{
            
            let indexPath = IndexPath(row: sender.tag, section: 0)
                let cell = ingredientstable.cellForRow(at: indexPath) as! ingredientTVCellTableViewCell
            
            if counts[indexPath.row] <= 1 {
                
            }else{
            
                counts[indexPath.row] -= 1
                
                let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
                let pricedata = dictObj["price"]as! String
                let priceINt = Double(pricedata)!
                let countInt = Double(counts[indexPath.row])
                let sumPP = priceINt * countInt
                cell.ingredientCountLBL.text = "\(counts[indexPath.row])"
                let rupee = "$"
                let convertdouble = String(format: "%.2f", sumPP)
                cell.ingredientPrice.text = rupee + convertdouble
                ingredientsQtyArr[indexPath.row] = counts[indexPath.row]
                IngredientConvertedPrice[indexPath.row] = sumPP
                print("quantityArray - \(ingredientsQtyArr)")
                print("IngredientConvertedPrice - \(IngredientConvertedPrice)")
                
                let totalQty = IngredientConvertedPrice.reduce(0, +)

                print("product unit price = \(productunitprice)")

                let addedprice = productunitprice + totalQty

             //   productunitprice = addedprice

                productSubtotalPrice.text = rupee + String(format: "%.2f", addedprice)
                
            }
            
           
            
           
            
        }
        
    }
    
    @IBAction func PlusClicked(_ sender: UIButton) {
        
        if selectiontagArray[sender.tag] == "NO"{
            
            showSimpleAlert(messagess: "Please select ingredient first")

        }else{
            
            let indexPath = IndexPath(row: sender.tag, section: 0)
                let cell = ingredientstable.cellForRow(at: indexPath) as! ingredientTVCellTableViewCell
                counts[indexPath.row] += 1
            
            let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
            let pricedata = dictObj["price"]as! String
            let priceINt = Double(pricedata)!
            let countInt = Double(counts[indexPath.row])
            let sumPP = priceINt * countInt
            cell.ingredientCountLBL.text = "\(counts[indexPath.row])"
            let rupee = "$"
            let convertdouble = String(format: "%.2f", sumPP)
            cell.ingredientPrice.text = rupee + convertdouble
            
            ingredientsQtyArr[indexPath.row] = counts[indexPath.row]
            IngredientConvertedPrice[indexPath.row] = sumPP
            print("quantityArray - \(ingredientsQtyArr)")
            print("IngredientConvertedPrice - \(IngredientConvertedPrice)")
            
            let totalQty = IngredientConvertedPrice.reduce(0, +)

            print("product unit price = \(productunitprice)")

            let addedprice = productunitprice + totalQty

        //    productunitprice = addedprice

            productSubtotalPrice.text = rupee + String(format: "%.2f", addedprice)
            
        }
        
        
    }
        
        // method to run when table view cell is tapped
   /*     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
               
                if selectiontagArray[indexPath.row] == "NO"{

                    let cell = tableView.cellForRow(at: indexPath) as! ingredientTVCellTableViewCell

                    let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
                    let pricedata = dictObj["price"]as! String

                    IngredientConvertedPrice[indexPath.row] = Double(pricedata)!
                    
                    let totalQty = IngredientConvertedPrice.reduce(0, +)

                    print("product unit price = \(productunitprice)")

                    let addedprice = productunitprice + totalQty

                 //   productunitprice = addedprice

                    let rupee = "$"

                    productSubtotalPrice.text = rupee + String(format: "%.2f", addedprice)
                    
                  
                    
                    cell.selctionimg.image = UIImage(named: "right.png")

                    
                    selectiontagArray[indexPath.row] = "YES"

                    print("selectatgarray - \(selectiontagArray)")
                    
                    ingredientsQtyArr[indexPath.row] = 1
                    counts[indexPath.row] += 1
                    
                }else{

                    let cell = tableView.cellForRow(at: indexPath) as! ingredientTVCellTableViewCell
                    
                    
                    let dictObj = self.ingrediantArray[indexPath.row] as! NSDictionary
                    let pricedata = dictObj["price"]as! String
 
                    counts[indexPath.row] = 0
                    IngredientConvertedPrice[indexPath.row] = 0.00
                    ingredientsQtyArr[indexPath.row] = 0
                    cell.ingredientCountLBL.text = "1"
                    
                    let totalQty = IngredientConvertedPrice.reduce(0, +)

                    print("product unit price = \(productunitprice)")

                    let addedprice = productunitprice + totalQty

                //    productunitprice = addedprice

                    let rupee = "$"

                    productSubtotalPrice.text = rupee + String(format: "%.2f", addedprice)

                    cell.selctionimg.image = UIImage(named: "blank-square.png")

             
                    selectiontagArray[indexPath.row] = "NO"
                    print("selectatgarray - \(selectiontagArray)")

                    cell.ingredientPrice.text = rupee + pricedata
                    
                    
                }
       
        }*/
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      
            return 52.0
   
        }
    


    
    @IBAction func valueChanged1(_ sender: ValueStepper) {
        print("Stepper 1: \(sender.value)")
        let qut = Int(sender.value)
        quantityNumber = String(qut)
      //  let first = Double(passunitprice)
        
        let first = Double(passunitprice)
        let second = Double(sender.value)
        
        var sum = Double()
        sum = first! * second
        
        productunitprice = sum
        
        let totalQty = IngredientConvertedPrice.reduce(0, +)
        
        let withingsum = totalQty + sum
        
     //   productSubtotalPrice.text = String(format: "%.2f", sum)
       
            let rupee = "$"
            
            productSubtotalPrice.text = rupee + String(format: "%.2f", withingsum)
        
    }
    
    
    //MARK: Webservice Call for add to cart

     
    func addToCart() {
        
        
        print(passproductid)
        
        let defaults = UserDefaults.standard
        
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        if defaults.object(forKey: "AvlbCartId") == nil {
          
            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
           
            
        }else{
        
         let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            let cartidStr = String(avlCartId)
            
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
            
            print("customer token -\(String(describing: admintoken))")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let urlString = GlobalObjects.DevlopmentApi+"cart-item/"
        
//        let headers: HTTPHeaders = [
//            "Content-Type": "application/json",
//            "Authorization": autho,
//            "user_id": customeridStr,
//            "cart_id": cartidStr,
//            "action": "cart-item"
//        ]

            let now = Date()

                let formatter = DateFormatter()

                formatter.timeZone = TimeZone.current

                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS" ///2020-10-19 12:05:10.625

                let dateString = formatter.string(from: now)
            
            print(dateString)
            var commentStr = String()
            
            if textbaseViewTf.text == nil || textbaseViewTf.text! == "" {
                commentStr = ""
            }else{
                
                commentStr = textbaseViewTf.text! as String
            }
            
           print("commentStr - \(commentStr)")
            
            let cartdatacount = defaults.object(forKey: "cartdatacount")as! String
           // let sendcartcount = cartdatacount
            
            let qtyy = Int(quantityNumber)
            
            let metadataDict = ["updated_at":dateString,"extra":commentStr, "quantity":qtyy! as Int, "cart_id":avlCartId as Any,"product_id":passproductid as Any,"ingredient_id":0,"sequence_id":cartdatacount]
          
            let productarr = [metadataDict]
            print("product element Dict - \(productarr)")
            
            var ingredientarray = Array<Any>()
            
            for (index, selectedIn) in selectiontagArray.enumerated() {
                
                let dictObj = self.ingrediantArray[index] as! NSDictionary
                let ingredientidd = dictObj["ingredient_id"]as! NSNumber
                let idddStr = ingredientidd.stringValue
                let checkingstr = selectedIn
                
                let individualingredientQty = ingredientsQtyArr[index]
                
                if checkingstr == "YES" {
                    
                    let ingredientDict = ["updated_at":dateString,"extra":"test", "quantity":individualingredientQty, "cart_id":avlCartId as Any,"product_id":passproductid as Any,            "ingredient_id":idddStr,"sequence_id":cartdatacount]
                    
                    ingredientarray.append(ingredientDict)
                    
                }
                
            }
        
            
            ingredientarray.insert(metadataDict, at: 0)
            print("Array product with ingredient added - \(ingredientarray)")

        
            let fileUrl = URL(string: urlString)

            var request = URLRequest(url: fileUrl!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(autho, forHTTPHeaderField: "Authorization")
            request.setValue(customeridStr, forHTTPHeaderField: "user_id")
            request.setValue(cartidStr, forHTTPHeaderField: "cart_id")
            request.setValue("cart-item", forHTTPHeaderField: "action")


            request.httpBody = try! JSONSerialization.data(withJSONObject: ingredientarray)

            AF.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    switch response.result {
                                            case .success:
                                                print(response)
                    
                                                if response.response?.statusCode == 201{
                    
                                                    ERProgressHud.sharedInstance.hide()
                    
                                                 let alert = UIAlertController(title: nil, message: "Product added successfully into the cart",         preferredStyle: UIAlertController.Style.alert)
                    
                    
                                                   alert.addAction(UIAlertAction(title: "OK",
                                                                                 style: UIAlertAction.Style.default,
                                                                                 handler: {(_: UIAlertAction!) in
                    
                                                          self.navigationController?.popViewController(animated: false)
                    
                                                   }))
                                                   self.present(alert, animated: true, completion: nil)
                                                    alert.view.tintColor = UIColor(rgb: 0xFC4355)
                    
                                                }else{
                    
                                                    if response.response?.statusCode == 401{
                    
                                                        ERProgressHud.sharedInstance.hide()
                                                        self.SessionAlert()
                    
                                                    }else if response.response?.statusCode == 500{
                    
                                                        ERProgressHud.sharedInstance.hide()
                    
                                                        let dict :NSDictionary = response.value! as! NSDictionary
                    
                                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                    }else{
                    
                                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                       }
                    
                                                }
                    
                                                break
                                            case .failure(let error):
                                                ERProgressHud.sharedInstance.hide()
                    
                                                print(error.localizedDescription)
                    
                                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                    
                                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                    
                                                let msgrs = "URLSessionTask failed with error: The request timed out."
                    
                                                if error.localizedDescription == msg {
                    
                                            self.showSimpleAlert(messagess:"No internet connection")
                    
                                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
                                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                    
                                                }else{
                    
                                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                                }
                    
                                                   print(error)
                                            }
            }
            
            
            
                
                
//            AF.request(urlString, method: .post, parameters:productarr,encoding: JSONEncoding.default, headers: headers).responseJSON {
//        response in
//          switch response.result {
//                        case .success:
//                            print(response)
//
//                            if response.response?.statusCode == 201{
//
//                                ERProgressHud.sharedInstance.hide()
//
//                             let alert = UIAlertController(title: nil, message: "Product added successfully into the cart",         preferredStyle: UIAlertController.Style.alert)
//
//
//                               alert.addAction(UIAlertAction(title: "OK",
//                                                             style: UIAlertAction.Style.default,
//                                                             handler: {(_: UIAlertAction!) in
//
//                                      self.navigationController?.popViewController(animated: true)
//
//                               }))
//                               self.present(alert, animated: true, completion: nil)
//                                alert.view.tintColor = UIColor(rgb: 0xFE9300)
//
//                            }else{
//
//                                if response.response?.statusCode == 401{
//
//                                    ERProgressHud.sharedInstance.hide()
//                                   // self.SessionAlert()
//
//                                }else if response.response?.statusCode == 500{
//
//                                    ERProgressHud.sharedInstance.hide()
//
//                                    let dict :NSDictionary = response.value! as! NSDictionary
//
//                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
//                                }else{
//
//                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
//                                   }
//
//                            }
//
//                            break
//                        case .failure(let error):
//                            ERProgressHud.sharedInstance.hide()
//
//                            print(error.localizedDescription)
//
//                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
//
//                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
//
//                            let msgrs = "URLSessionTask failed with error: The request timed out."
//
//                            if error.localizedDescription == msg {
//
//                        self.showSimpleAlert(messagess:"No internet connection")
//
//                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
//
//                        self.showSimpleAlert(messagess:"Slow Internet Detected")
//
//                            }else{
//
//                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
//                            }
//
//                               print(error)
//                        }
//        }
    }

     }
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: messagess, message: nil,         preferredStyle: UIAlertController.Style.alert)

      //  alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action//
      //  }))
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
        }))
        
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFC4355)
    }
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        self.performSegue(withIdentifier: "backlogin", sender: self)
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFC4355)
    }
    
    
    
  
    func GetIngredientList(){
        
        var admintoken = String()
        let defaults = UserDefaults.standard
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
       
            admintoken = (defaults.object(forKey: "adminToken")as? String)!
            
        }else{
            
            admintoken = (defaults.object(forKey: "custToken")as? String)!
            
        }
    
        let autho = "token \(admintoken)"
        
        let urlString = GlobalObjects.DevlopmentApi+"ingredient/?product_id=\(passproductid)&status=ACTIVE"
        
           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                 
                                    let dict :NSDictionary = response.value! as! NSDictionary

                                    let ingredientsList:NSArray = dict.value(forKey: "results")as! NSArray

                                    
                                    if ingredientsList.count == 0 {
                                    
                                        self.ingredientlbl.isHidden = true
                                        self.ingredientTableheight.constant = 0
                                        self.ingrediantArray = []
                                        self.selectiontagArray = []
                                        self.ingredientstable.reloadData()
                                        self.top1space.constant = 0
                                        self.top2space.constant = 0
                                        self.top3space.constant = 0
                                        
                                    }else{
                                        
                                        for index in ingredientsList {
                                            
                                            let settag = "NO"
                                            let qty = 0
                                            let ingPrice = 0.00
                                            self.selectiontagArray.append(settag)
                                            self.ingredientsQtyArr.append(qty)
                                            self.IngredientConvertedPrice.append(ingPrice)
                                        }
                                        
                                        print("selectiontag - \(self.selectiontagArray)")
                                        
                                        self.ingredientlbl.isHidden = false
                                        let sun = ingredientsList.count
                                self.ingredientTableheight.constant = CGFloat(52 * Int(sun))
                                        
                                        self.ingrediantArray = ingredientsList
                                        self.ingredientstable.reloadData()
                                        
                                    }
                                    
                                    
                                    
                                    print("ingredientsList = \(ingredientsList)")
                                    
                                    ERProgressHud.sharedInstance.hide()

                                 
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                    self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
  
    //MARK: check cart Section
    
    //MARK: check cart Api
       
        func checkCartAvl()  {
           
           let defaults = UserDefaults.standard
           
           let savedUserData = defaults.object(forKey: "custToken")as? String
           
           let customerid = defaults.integer(forKey: "custId")
           let custidStr = String(customerid)
           
           let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
               
           let urlString = GlobalObjects.DevlopmentApi+"cart/?customer_id="+custidStr+""
           print("Url cust avl - \(urlString)")
               
               let headers: HTTPHeaders = [
                   "Content-Type": "application/json",
                   "Authorization": token,
                   "user_id": custidStr,
                   "action": "cart"
               ]

                AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
               response in
                 switch response.result {
                               case .success:
                                   
                                 //  print(response)

                                   if response.response?.statusCode == 200{
                                     //  self.dissmiss()
                                       
                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                    //  print(dict)
                                       
                                       let status = dict.value(forKey: "results")as! NSArray
                                                      print(status)
                                                        
                                           print("store available or not - \(status)")
                                       
                                       
                                                        
                   if status.count == 0 {
                       
                  //  ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                       self.createCart()
                                                           
                   }else{
                            
                               let firstobj:NSDictionary  = status.object(at: 0) as! NSDictionary

                               let getAvbcartId = firstobj["id"] as! Int
                        let storeWRTCart = firstobj["restaurant"] as! Int


                        let defaults = UserDefaults.standard

                               defaults.set(getAvbcartId, forKey: "AvlbCartId")
                               defaults.set(storeWRTCart, forKey: "storeIdWRTCart")


                                    print("avl cart id - \(getAvbcartId)")
                                    print("cart w R to store  - \(storeWRTCart)")


                   // self.GetIngredientList()
                  //  self.addToCart()

               }
                     
               }else{
                                       
                  // self.dissmiss()
                   print(response)
                   if response.response?.statusCode == 401{
                                          
                       self.SessionAlert()
                                           
                    }else if response.response?.statusCode == 500{
                                                                      
                 //  self.dissmiss()
                                                                      
                                                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                                      
                                                                      self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                                  }else{
                                                                   
                                                                   self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                                  }
               }
                                   
                                   break
                               case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
               }
               
         
               
           }
       
       
    
    func createCart() {
        
        
        let defaults = UserDefaults.standard
        
        let savedUserData = defaults.object(forKey: "custToken")as? String
         let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
       
        let customerid = defaults.integer(forKey: "custId")
        let custidStr = String(customerid)
       

//        let urlString = GlobalObjects.DevlopmentApi+"cart/?customer_id=\(customerid)&restaurant_id=\(passedrestaurantid)"

        let urlString = GlobalObjects.DevlopmentApi+"cart/"
        
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": token,
            "user_id": custidStr,
            "action": "cart"
        ]
        
        AF.request(urlString, method: .post, parameters: ["customer_id":customerid,"restaurant":GlobalObjects.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                                                 //   self.dissmiss()
                                                    
                                                    let resultarr :NSDictionary = response.value! as! NSDictionary
                                
                                                print(resultarr)
                                
                                
                             //   let resultarr : NSArray = dict.value(forKey: "results") as! NSArray
                                
                                if resultarr.count == 0 {
                                  
                                    self.showSimpleAlert(messagess: "Cart not created")
                                    
                                }else{
                                
                             //   let dictObj = resultarr[0] as! NSDictionary

                                                    
                                                    let getAvbcartId = resultarr["id"] as! Int
                                                                                                      let storeWRTCart = resultarr["restaurant"] as! Int
                                                                                                      
                                                                                                      let defaults = UserDefaults.standard
                                                                                                      
                                                                                                      defaults.set(getAvbcartId, forKey: "AvlbCartId")
                                                                                                      defaults.set(storeWRTCart, forKey: "storeIdWRTCart")
                                                                                                           
                                                                                                           print("avl cart id - \(getAvbcartId)")
                                                                                                           print("cart w R to store  - \(storeWRTCart)")
                                                                     
                            //    self.dissmiss()
                                  
                       //     self.performSegue(withIdentifier: "productListVC", sender: self)
                                    
                                }
                                
                            //    ERProgressHud.sharedInstance.hide()

                                self.GetIngredientList()
                                
                            }else{
                             
                              if response.response?.statusCode == 500{
                                                            
                                   //                         self.dissmiss()
                                                            
                                                             let dict :NSDictionary = response.value! as! NSDictionary
                                                            
                                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                        }else{
                                                            
                                                      //      self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                           }
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }


     }
    
    
    
}
